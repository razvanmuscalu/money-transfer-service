# money-transfer-service

RESTful API (including data model and the backing implementation) for money transfers between accounts.

# Overview

API

- make transfer via POST

```
POST localhost:9090/transfers

{
	"amount": 100,
	"from": "user-1",
	"to": "user-2"
}
```

- get successful transfer by ID via GET

`GET localhost:9090/transfers/<id>`

# Design Considerations

I built the transfer-service assuming a micro-service architecture. Therefore, user-service would probably be another service with which transfer-service would communicate via HTTP (for a sync architecture) or Kafka (for an async architecture) or etc.

`TransfersStore` and `AccountStore` would not be external services, but instead would be local databases of the transfer-service.

This is why I opted not to add unit tests for `AccountStore`, `UserService` and `TransfersStore`, as these unit tests would test my dummy in-memory implementations which is not how the system would behave in reality.

# Other features given more time

The data models are the simplest to achieve a minimum viable product for transfer-service.

- `Transfer` could have currency, etc.
- `RecordedTransfer` could have timestamp, etc.
- `BankAccount` could have type, currency, bank, etc.
- `User` could have date of birth, address, etc.

Given these more complex data models, API could be extended to support:

- get failed transfers
- get transfers made by user
- get transfers received by user
- get transfers by timestamp, currency, etc.

# packaging

- run `mvn package` and an executable JAR will be created under `target/docker-src`

# Thread Safety

Because I made `AccountStore` work on top of an in-memory map, I had to lock access to this map in `TransferService` via the `Locker` object. In reality, `AccountStore` would work on top of a database, and the lock in `TransferService` would not be required. Instead, the two calls to `updateBalance` would have to be performed as a database transaction.

For example, with Slick this would be something like:

```
(for {
    _ <- accountStore.updateBalance(from.account, -transfer.amount)
    _ <- accountStore.updateBalance(to.account, transfer.amount)
} yield ()).transactionally
```

Furthermore, the call to `put` the transfer into the `TransfersStore` could also be placed inside the same transaction like so:

```
(for {
    _ <- accountStore.updateBalance(from.account, -transfer.amount)
    _ <- accountStore.updateBalance(to.account, transfer.amount)
    res <- 
        val transferOutcome = TransferOutcome(success = true, UUID.randomUUID().toString, "Transfer has been successful")
        transfersStore.put(transfer, transferOutcome)
        transferOutcome
} yield res).transactionally
```

This is if assuming that `TransfersStore` is using the same database as `AccountStore`. I have not done all of this (and instead implemented the operation to save the transfer as a fire-and-forget operation because the instructions mentioned to not use a real database)

Similarly, I implemented `UserService` on top of an in-memory map, but in reality this would perform a call to an external service as specified above.