package com.revolut.transfer.service

import java.util.UUID

import com.revolut.transfer.SysBaseTest
import com.revolut.transfer.model._
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar

class TransferServiceTest extends SysBaseTest with MockitoSugar with BeforeAndAfterEach {
  private val testUser1 = User("user-1", "John", "West", "user-1-account")
  private val testUser2 = User("user-2", "Maria", "North", "user-2-account")

  private val testTransfer = Transfer(100, "user-1", "user-2")
  private val testTransferOutcome = TransferOutcome(success = true, "transfer-1", "Transfer has been successful")

  implicit private val userService: UserService = mock[UserService]
  implicit private val transfersStore: TransfersStore = mock[TransfersStore]
  implicit private val accountStore: AccountStore = new AccountStoreImpl
  private val transferService = new TransferServiceImpl

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    reset(userService, transfersStore)
    when(userService.get("user-1")).thenReturn(Option(testUser1))
    when(userService.get("user-2")).thenReturn(Option(testUser2))
  }

  "TransferService (make)" should {
    "return success outcome" in {
      val resp = transferService.make(testTransfer)
      resp.success shouldBe true
    }

    "return unique reference" in {
      UUID.fromString(transferService.make(testTransfer).reference) // should not throw exception
    }

    "return failed outcome when amount is negative" in {
      val transfer = testTransfer.copy(amount = -100)
      val resp = transferService.make(transfer)
      resp.success shouldBe false
      resp.message shouldBe "Amount -100.0 has to be bigger than 0"
      verify(transfersStore).put(any[Transfer], any[TransferOutcome])
    }

    "return failed outcome when sender not found" in {
      when(userService.get("user-1")).thenReturn(None)
      val resp = transferService.make(testTransfer)
      resp.success shouldBe false
      resp.message shouldBe "User user-1 not found"
      verify(transfersStore).put(any[Transfer], any[TransferOutcome])
    }

    "return failed outcome when recipient not found" in {
      when(userService.get("user-2")).thenReturn(None)
      val resp = transferService.make(testTransfer)
      resp.success shouldBe false
      resp.message shouldBe "User user-2 not found"
      verify(transfersStore).put(any[Transfer], any[TransferOutcome])
    }

    "return failed outcome when neither sender nor recipient not found" in {
      when(userService.get("user-1")).thenReturn(None)
      when(userService.get("user-2")).thenReturn(None)
      val resp = transferService.make(testTransfer)
      resp.success shouldBe false
      resp.message shouldBe "Users user-1 and user-2 not found"
      verify(transfersStore).put(any[Transfer], any[TransferOutcome])
    }

    "return failed outcome when sender balance is too low" in {
      val transfer = testTransfer.copy(amount = 5000)
      val resp = transferService.make(transfer)
      resp.success shouldBe false
      resp.message shouldBe "Balance for user John West is too low"
      verify(transfersStore).put(any[Transfer], any[TransferOutcome])
    }

    "make transfer and debit sender's balance" in {
      val beforeBalance = accountStore.getBalance(testUser1.account).amount

      transferService.make(testTransfer)
      val afterBalance = accountStore.getBalance(testUser1.account).amount

      afterBalance - beforeBalance shouldBe -100
    }

    "make transfer and credit recipient's balance" in {
      val beforeBalance = accountStore.getBalance(testUser2.account).amount

      transferService.make(testTransfer)
      val afterBalance = accountStore.getBalance(testUser2.account).amount

      afterBalance - beforeBalance shouldBe 100
    }

    "perform 200 transfers with amount of 1 concurrently" in {
      (1 to 10).foreach { _ =>
        implicit val accountStore: AccountStore = new AccountStoreImpl
        val transferService = new TransferServiceImpl

        val t1 = new Thread {
          override def run(): Unit =
            (1 to 100).foreach { _ =>
              transferService.make(Transfer(1, "user-1", "user-2"))
            }
        }

        val t2 = new Thread {
          override def run(): Unit =
            (1 to 100).foreach { _ =>
              transferService.make(Transfer(1, "user-1", "user-2"))
            }
        }

        val beforeBalance = accountStore.getBalance(testUser2.account).amount

        t1.start()
        t2.start()

        Thread.sleep(500)

        val afterBalance = accountStore.getBalance(testUser2.account).amount

        afterBalance - beforeBalance shouldBe 200

      }
    }

    "add successful transfer to store" in {
      transferService.make(testTransfer)
      verify(transfersStore).put(any[Transfer], any[TransferOutcome])
    }
  }

  "TransferService (get)" should {
    "return None if requested transfer does not exist" in {
      when(transfersStore.get("transfer-1")).thenReturn(None)
      transferService.get("transfer-1") shouldBe None
    }

    "return recorded transfer" in {
      when(transfersStore.get("transfer-1")).thenReturn(Option(RecordedTransfer(testTransfer, testTransferOutcome)))
      val resp = transferService.get("transfer-1")
      resp.get.transfer shouldBe testTransfer
      resp.get.transferOutcome shouldBe testTransferOutcome
    }
  }
}
