package com.revolut.transfer.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes.{BadRequest, NotFound, OK}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.revolut.transfer.BaseTest
import com.revolut.transfer.model.{RecordedTransfer, Transfer, TransferOutcome}
import com.revolut.transfer.service.TransferService
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import spray.json._

class TransferRouteTest extends BaseTest with ScalatestRouteTest with MockitoSugar with BeforeAndAfterEach {
  private val testTransfer = read("/mocks/transfer.json").parseJson.convertTo[Transfer]
  private val testTransferOutcome = TransferOutcome(success = true, "transfer-1", "This has been successful")
  private val testRecordedTransfer = RecordedTransfer(testTransfer, testTransferOutcome)

  private val transferService = mock[TransferService]
  private val route: Route = TransfersRoute.transferRoute(transferService)

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    reset(transferService)
    when(transferService.make(testTransfer)).thenReturn(testTransferOutcome)
    when(transferService.get("transfer-1")).thenReturn(Option(RecordedTransfer(testTransfer, testTransferOutcome)))
  }

  "TransferRoute (POST)" should {
    "return 200 OK" in {
      Post("/transfers", read("/mocks/transfer.json").parseJson) ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "return 404 NotFound when asking for non-existent endpoint" in {
      Post("/unknown", read("/mocks/transfer.json").parseJson) ~> Route.seal(route) ~> check {
        status shouldBe NotFound
      }
    }

    "return 400 BadRequest when asking with invalid request body" in {
      Post("/transfers", read("/mocks/transfer-missing-balance.json").parseJson) ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "call service with request body" in {
      Post("/transfers", read("/mocks/transfer.json").parseJson) ~> Route.seal(route) ~> check {
        verify(transferService).make(testTransfer)
      }
    }

    "return response" in {
      Post("/transfers", read("/mocks/transfer.json").parseJson) ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[TransferOutcome] shouldBe testTransferOutcome
      }
    }
  }

  "TransferRoute (GET)" should {
    "return 200 OK" in {
      Get("/transfers/transfer-1") ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "return 404 NotFound when asking for non-existent endpoint" in {
      Get("/unknown/transfer-1") ~> Route.seal(route) ~> check {
        status shouldBe NotFound
      }
    }

    "return 404 NotFound when asking for non-existent resource" in {
      when(transferService.get("transfer-1")).thenReturn(None)
      Get("/transfers/transfer-1") ~> Route.seal(route) ~> check {
        status shouldBe NotFound
      }
    }

    "call service with id" in {
      Get("/transfers/transfer-1") ~> Route.seal(route) ~> check {
        verify(transferService).get("transfer-1")
      }
    }

    "return response" in {
      Get("/transfers/transfer-1") ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[RecordedTransfer] shouldBe testRecordedTransfer
      }
    }
  }

}
