package com.revolut.transfer

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.revolut.transfer.http.TransfersRoute
import com.revolut.transfer.service._

import scala.language.postfixOps

trait AppCake extends Bootstrap with AppConfig {
  this: Sys =>

  implicit private val userService: UserService = new UserServiceImpl
  implicit private val accountStore: AccountStore = new AccountStoreImpl
  implicit private val transfersStore: TransfersStore = new TransfersStoreImpl
  private val transferService: TransferService = new TransferServiceImpl
  val route: Route = TransfersRoute.transferRoute(transferService)
}

object Main extends App {
  private lazy val system: ActorSystem = ActorSystem()

  val sys = new SysProvider()(system, ActorMaterializer()(system)) with AppCake
  sys.start(sys.route)
}
