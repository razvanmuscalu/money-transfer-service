package com.revolut.transfer.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.revolut.transfer.model.Transfer
import com.revolut.transfer.service.TransferService
import spray.json._

import scala.concurrent.ExecutionContext

object TransfersRoute {
  def transferRoute(transferService: TransferService)(implicit ec: ExecutionContext): Route =
    (path("transfers") & post) {
      entity(as[Transfer]) { payload =>
        complete {
          val resp = transferService.make(payload)
          HttpEntity(`application/json`, resp.toJson.compactPrint)
        }
      }
    } ~ (path("transfers" / Segment) & get) { id =>
      rejectEmptyResponse {
        complete {
          transferService.get(id).map(resp => HttpEntity(`application/json`, resp.toJson.compactPrint))
        }
      }
    }
}
