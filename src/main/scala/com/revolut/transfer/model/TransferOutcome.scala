package com.revolut.transfer.model

import spray.json.RootJsonFormat

final case class TransferOutcome(success: Boolean, reference: String, message: String)

object TransferOutcome {
  import spray.json.DefaultJsonProtocol._

  implicit val transferOutcomeFormat: RootJsonFormat[TransferOutcome] = jsonFormat3(TransferOutcome.apply)
}