package com.revolut.transfer.model

final case class User(id: String, firstName: String, lastName: String, account: String)
