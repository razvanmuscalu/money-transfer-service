package com.revolut.transfer.model

final case class BankAccount(amount: Double)
