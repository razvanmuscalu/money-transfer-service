package com.revolut.transfer.model

import spray.json.RootJsonFormat

final case class RecordedTransfer(transfer: Transfer, transferOutcome: TransferOutcome)

object RecordedTransfer {
  import spray.json.DefaultJsonProtocol._
  import Transfer.transferFormat
  import TransferOutcome.transferOutcomeFormat

  implicit val recordedTransferFormat: RootJsonFormat[RecordedTransfer] = jsonFormat2(RecordedTransfer.apply)
}