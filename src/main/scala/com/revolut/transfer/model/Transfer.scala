package com.revolut.transfer.model

import spray.json.RootJsonFormat

final case class Transfer(amount: Double, from: String, to: String)

object Transfer {
  import spray.json.DefaultJsonProtocol._

  implicit val transferFormat: RootJsonFormat[Transfer] = jsonFormat3(Transfer.apply)
}