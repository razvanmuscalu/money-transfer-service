package com.revolut.transfer.service

import java.util.concurrent.ConcurrentHashMap

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.revolut.transfer.model._

import scala.concurrent.ExecutionContext

trait TransfersStore {
  def get(id: String): Option[RecordedTransfer]

  def put(transfer: Transfer, transferOutcome: TransferOutcome): Unit
}

class TransfersStoreImpl()(implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer) extends TransfersStore {
  private val inMemStore: ConcurrentHashMap[String, RecordedTransfer] = new ConcurrentHashMap()

  def get(id: String): Option[RecordedTransfer] = {
    val transfer = inMemStore.get(id)

    if (transfer == null) {
      None
    } else {
      Option(transfer)
    }
  }

  override def put(transfer: Transfer, transferOutcome: TransferOutcome): Unit =
    inMemStore.put(transferOutcome.reference, RecordedTransfer(transfer, transferOutcome))
}
