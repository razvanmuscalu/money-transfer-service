package com.revolut.transfer.service

import java.util.concurrent.ConcurrentHashMap

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.revolut.transfer.model._

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

trait AccountStore {
  def getBalance(id: String): BankAccount

  def updateBalance(id: String, amount: Double): BankAccount
}

class AccountStoreImpl()(implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer) extends AccountStore {
  private val inMemStore: ConcurrentHashMap[String, BankAccount] = new ConcurrentHashMap(
    Map(
      "user-1-account" -> BankAccount(1000),
      "user-2-account" -> BankAccount(2000)
    ).asJava
  )

  def getBalance(id: String): BankAccount =
    inMemStore.get(id)

  def updateBalance(id: String, amount: Double): BankAccount = {
    val account = inMemStore.get(id)
    val updatedAccount = account.copy(amount = account.amount + amount)
    inMemStore.put(id, updatedAccount)
  }
}
