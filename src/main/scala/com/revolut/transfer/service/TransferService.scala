package com.revolut.transfer.service

import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.revolut.transfer.model._
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

trait TransferService {
  def make(transfer: Transfer): TransferOutcome

  def get(id: String): Option[RecordedTransfer]
}

class TransferServiceImpl()(implicit userService: UserService,
                            accountStore: AccountStore,
                            transfersStore: TransfersStore,
                            system: ActorSystem,
                            ec: ExecutionContext,
                            mat: Materializer)
    extends TransferService {

  private val logger: Logger = LoggerFactory.getLogger(getClass)

  private object Locker

  def make(transfer: Transfer): TransferOutcome =
    transfer.amount match {
      case x if x < 0 => failTransfer(transfer, s"Amount $x has to be bigger than 0")
      case _ =>
        (userService.get(transfer.from), userService.get(transfer.to)) match {
          case (Some(from), Some(to)) => makeTransfer(from, to, transfer)
          case (_, Some(_))           => failTransfer(transfer, s"User ${transfer.from} not found")
          case (Some(_), _)           => failTransfer(transfer, s"User ${transfer.to} not found")
          case (_, _)                 => failTransfer(transfer, s"Users ${transfer.from} and ${transfer.to} not found")
        }
    }

  def get(id: String): Option[RecordedTransfer] = transfersStore.get(id)

  private def makeTransfer(from: User, to: User, transfer: Transfer): TransferOutcome =
    accountStore.getBalance(from.account) match {
      case x if x.amount < transfer.amount =>
        failTransfer(transfer, s"Balance for user ${from.firstName} ${from.lastName} is too low")
      case _ =>
        Locker.synchronized {
          accountStore.updateBalance(from.account, -transfer.amount)
          accountStore.updateBalance(to.account, transfer.amount)
        }

        val transferOutcome = TransferOutcome(success = true, UUID.randomUUID().toString, "Transfer has been successful")
        transfersStore.put(transfer, transferOutcome)

        transferOutcome
    }

  private def failTransfer(transfer: Transfer, message: String): TransferOutcome = {
    logger.warn(message)
    val transferOutcome = TransferOutcome(success = false, UUID.randomUUID().toString, message)
    transfersStore.put(transfer, transferOutcome)
    transferOutcome
  }
}
