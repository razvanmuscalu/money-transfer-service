package com.revolut.transfer.service

import java.util.concurrent.ConcurrentHashMap

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.revolut.transfer.model._

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

trait UserService {
  def get(id: String): Option[User]
}

class UserServiceImpl()(implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer) extends UserService {
  private val inMemStore: ConcurrentHashMap[String, User] = new ConcurrentHashMap(
    Map(
      "user-1" -> User("user-1", "John", "West", "user-1-account"),
      "user-2" -> User("user-2", "Maria", "North", "user-2-account")
    ).asJava
  )

  def get(id: String): Option[User] = {
    val user = inMemStore.get(id)

    if (user == null) {
      None
    } else {
      Option(user)
    }
  }
}
