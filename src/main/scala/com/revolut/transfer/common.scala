package com.revolut.transfer

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.language.postfixOps
import scala.util.{Failure, Success}

trait Sys {
  implicit val system: ActorSystem
  implicit lazy val ec: ExecutionContextExecutor = system.dispatcher
  implicit val mat: Materializer
}

class SysProvider(implicit val system: ActorSystem, implicit val mat: Materializer) extends Sys

trait AppConfig {
  final val started: Long = System.currentTimeMillis()

  val config: Config = ConfigFactory.load()

  private val commonConfig: Config = config.getConfig("common")

  val name: String = commonConfig.getString("name")
  val port: Int = commonConfig.getInt("port")
}

trait Bootstrap {
  this: Sys with AppConfig =>

  private val logger: Logger = LoggerFactory.getLogger(getClass)

  def start(route: Route): Unit = {
    logger.info(s"Initialisation done; starting HTTP server on port $port")
    Http().bindAndHandle(route, "localhost", port).onComplete {
      case Success(binding) =>
        logger.info(s"Service $name up and running; registering shutdown hook")
        sys.addShutdownHook({
          logger.info(s"Shutting down service: unbinding port $port")
          binding.unbind().onComplete {
            case Success(_) =>
              logger.debug(s"Port $port freed")
              terminate()
            case Failure(e) =>
              logger.error(s"Failed to free port $port", e)
              terminate()
          }
        })
        logger.debug("Hook registered")
      case Failure(e) =>
        logger.error("Failed to start HTTP service.  Shutting down", e)
        terminate()
    }
  }

  private def terminate(): Unit = {
    logger.info("Terminating actor system with 2 minutes wait time.")
    Await.result(system.terminate(), 2 minutes)
    logger.debug(s"Service $name Actor system has terminated")
  }
}
